import java.util.Objects;

@FunctionalInterface
interface Vehicle {
    void vehicleType();
}

class Car implements Vehicle {
    @Override
    public void vehicleType() {
        System.out.println("It is car");
    }
}

class Bike implements Vehicle {
    @Override
    public void vehicleType() {
        System.out.println("It is bike");
    }
}

class Boat implements Vehicle {
    @Override
    public void vehicleType() {
        System.out.println("It is baot");
    }
}

class VehiclesFactory {
    public Vehicle propOfVehicle(String type) {
        if (type.equals("Car"))
            return new Car();
        else if ("Bike".equals(type))
            return new Bike();
        else if (type == "Baot")
            return new Boat();
        return null;
    }
}

public class Main {
    public static void main(String[] args) {
//        Vehicle car = VehiclesFactory.propOfVehicle("Car");
        VehiclesFactory vehiclesFactory = new VehiclesFactory();
        Vehicle vehicle = vehiclesFactory.propOfVehicle("Baot");
        vehicle.vehicleType();
    }
}
