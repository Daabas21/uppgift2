;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args) {

        Predicate<String> vawel = Pattern.compile("^[^aeiou]*[aeiou][^aeiou]*[aeiou][^aeiou]*$*",Pattern.CASE_INSENSITIVE)
                .asPredicate();

        List.of("FInish","get","bring","subscribe","clean","drive","help","this","these","plz","congratulations")
                .stream().filter(vawel).forEach(System.out::println);



    }
}
