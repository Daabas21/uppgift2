import java.util.*;
import java.util.stream.Collectors;

class Person {

    private String name;
    private String gender;
    private double salary;

    public Person(String name, String gender, double salary) {
        this.name = name;
        this.gender = gender;
        this.salary = salary;
    }

    Person mergeSalary(Person person) {
        return new Person(this.name + " " + person.getName(),
                person.getGender(),
                this.salary + person.salary);
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", salary=" + salary +
                '}';
    }
}
public class Main {
    public static void main(String[] args) {

        List<Person> persons = List.of(new Person("Kalle", "male", 25000),
                new Person("Agda", "female", 23000),
                new Person("Ali", "male", 31000),
                new Person("Hassan", "male", 35000),
                new Person("Max", "male", 29000),
                new Person("Laila", "female", 42000),
                new Person("Sally", "female", 33000),
                new Person("Sandy", "female", 33000),
                new Person(" Yousef", "male", 52000),
                new Person("Stev", "male", 46000));


        List<Person> female = persons.stream()
                .filter(x -> x.getGender().contains("female"))
                .toList();

        int numberOfFemales = female.size();

        List<Double> femalesSalary = female.stream()
                .reduce(Person::mergeSalary)
                .map(Person::getSalary)
                .stream().toList();

        System.out.println("Average salary for females");
        System.out.println(femalesSalary.get(0) / numberOfFemales);

        List<Person> male = persons.stream()
                .filter(x -> x.getGender().equals("male"))
                .collect(Collectors.toList());

        int numberOfMales = male.size();

        List<Double> malesSalary = male.stream()
                .reduce(Person::mergeSalary)
                .map(Person::getSalary)
                .stream().toList();

        System.out.println("Average salary for males");
        System.out.println(malesSalary.get(0) / numberOfMales);

        System.out.println("Highest salary is for");
        System.out.println(persons.stream()
                .sorted(Comparator.comparing(Person::getSalary).reversed())
                .limit(1)
                .toList()
        );

        System.out.println("Lowest salary is");
        System.out.println(persons.stream()
                .sorted(Comparator.comparing(Person::getSalary))
                .limit(1)
                .toList()
        );
    }


}
