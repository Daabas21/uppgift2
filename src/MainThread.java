class PrimeNumberPrinter implements Runnable {
    int start;
    int end;
    int primenum = 0;


    public PrimeNumberPrinter(int start, int end, int primenum) {
        this.start = start;
        this.end = end;
        this.primenum = primenum;
    }

    boolean isPrime(int i) {
        final int upperLimit = (int) Math.sqrt(i);
        for (int j = 2; j <= upperLimit; j++) {
            if (i % j == 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void run() {
        for (int i = start; i <= end; i++) {
            if (isPrime(i)) {
                primenum++;
            }
        }
        System.out.println(primenum);
    }
}

public class MainThread {
    public static void main(String[] args) {

        Thread thread1 = new Thread(new PrimeNumberPrinter(0, 350000, 0));
        Thread thread2 = new Thread(new PrimeNumberPrinter(350001, 500000, 0));
        thread1.start();
        thread2.start();
    }
}
